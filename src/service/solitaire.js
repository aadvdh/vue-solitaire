import Deck from "../models/Deck";

export default class Solitaire {
  constructor() {
    this.piles = [[], [], [], [], [], [], []];
    this.foundations = [[], [], [], []];
    this.stocke = [];
    this.waste = [];

    this.initBoard();
  }
  initBoard() {
    let deck = new Deck().shuffle();

    //fill piles with cards
    for (let i = 0; i < this.piles.length; i++) {
      for (let j = i; j < this.piles.length; j++) {
        this.piles[i].push(deck.deal());
      }
    }

    //flip the top card
    this.piles.forEach((pile) => {
      let last = pile.length - 1;
      pile[last].flipCard();
    });

    //reverse piles so shortest pile is first
    this.piles = this.piles.reverse();
    //remainder of the deck is given to the stock
    this.stock = deck.deck;
  }

  moveFromWasteToFoundation(foundation) {
    const wasteCard = this.waste[this.waste.length - 1];

    if (!foundation || !this.canMoveToFoundation(foundation, wasteCard)) {
      //check if card can be placed onto other foundations;
      for (let f = 0; f < this.foundations.length; f++) {
        if (this.canMoveToFoundation(f, wasteCard)) {
          foundation = f;
          break;
        }
      }
    }
    //remove topcard of waste pile and move it to the foundation
    if (this.canMoveToFoundation(foundation, wasteCard)) {
      this.foundations[foundation].push(this.waste.pop());
    }
  }

  moveFromWasteToPile(pile) {
    const wasteCard = this.waste[this.waste.length - 1];
    if (this.canMoveToPile(pile, wasteCard)) {
      this.piles[pile].push(this.waste.pop());
    }
  }

  moveFromStockToWaste() {
    this.waste.push(this.stock.pop());
    this.waste[this.waste.length - 1].flipCard();
  }
  moveFromWasteToStock() {
    this.stock = this.waste.reverse();
    this.waste = [];
  }
  pickFromStock() {
    //while we still have in stock we move to watse
    if (this.stock.length > 0) {
      this.moveFromStockToWaste();
      //if we dont have stock and theres still waste, we move the waste back to the stock
    } else if (this.waste.length > 0) {
      this.moveFromWasteToStock();
      //all cards have to be down in the stock
      for (let i = 0; i < this.stock.length; i++) {
        if (!this.stock[i].isDown) {
          this.stock[i].flipCard();
        }
      }
    }
  }

  moveFromPileToFoundation(pile, foundation) {
    const source = this.piles[pile];
    const pileTopCard = source[source.length - 1];
    if (!foundation || !this.canMoveToFoundation(foundation, pileTopCard)) {
      for (var f = 0; f < this.foundations.length; f++) {
        if (this.canMoveToFoundation(f, pileTopCard)) {
          foundation = f;
          break;
        }
      }
    }
    if (this.canMoveToFoundation(foundation, pileTopCard)) {
      this.foundations[foundation].push(this.piles[pile].pop());
      //if old pile still contains a card, we flip it
      if (this.piles[pile].length != 0) {
        if (this.piles[pile][this.piles[pile].length - 1].isDown) {
          this.piles[pile][this.piles[pile].length - 1].flipCard();
        }
      }
    }
  }

  moveFromPileToPile(from, to, cardIndex) {
    const card = this.piles[from][cardIndex];
    if (this.canMoveToPile(to, card)) {
      const toMove = this.piles[from].splice(
        cardIndex,
        this.piles[from].length
      );
      toMove.forEach((card) => this.piles[to].push(card));

      //if old pile still contains a card, we flip it
      if (this.piles[from].length != 0) {
        if (this.piles[from][this.piles[from].length - 1].isDown) {
          this.piles[from][this.piles[from].length - 1].flipCard();
        }
      }
    }
  }
  moveFromFoundationToPile(foundation, pile) {
    const card = this.foundations[foundation][
      this.foundations[foundation].length - 1
    ];
    if (this.canMoveToPile(pile, card)) {
      this.piles[pile].push(this.foundations[foundation].pop());
    }
  }

  canMoveToPile(pile, card) {
    //if the pile is empty, only a king can be placed
    const destination = this.piles[pile];
    if (destination.length === 0) {
      return card.value === 13;
    }

    //check if they are not in sequence or are the same color
    const pileTopCard = destination[destination.length - 1];
    if (
      this.isInSequence(card, pileTopCard) &&
      !this.areSameColor(card, pileTopCard)
    ) {
      return true;
    }
    return false;
  }

  canMoveToFoundation(foundation, card) {
    const destination = this.foundations[foundation];
    // if foundation is empty, an ace needs to be placed
    if (destination.length == 0) {
      return card.value == 1;
    }

    const foundationTopCard = destination[destination.length - 1];
    if (
      card.suit == foundationTopCard.suit &&
      this.isInSequence(foundationTopCard, card)
    ) {
      return true;
    }

    return false;
  }

  hasPlayerWon() {
    for (let i = 0; i < this.foundations.length; i++) {
      if (this.foundations[i].length != 13) {
        return false;
      }
    }
    return true;
  }

  isInSequence(lower, higher) {
    return lower.value + 1 === higher.value;
  }
  areSameColor(card1, card2) {
    return this.suitColor(card1) === this.suitColor(card2);
  }
  suitColor(card) {
    return card.suit == "Clubs" || card.suit == "Spades" ? "black" : "red";
  }
  clearBoard() {
    this.piles = [[], [], [], [], [], [], []];
    this.stock = [];
    this.waste = [];
    this.foundations = [[], [], [], []];
  }

  restartGame() {
    this.clearBoard();
    this.initBoard();
  }

  state() {
    return {
      foundations: this.foundations,
      piles: this.piles,
      stock: this.stock,
      waste: this.waste,
    };
  }
}
