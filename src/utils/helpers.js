export function shuffle(array) {
  let copy = [...array];
  let length = array.length;
  let t, i;
  while (length) {
    i = Math.floor(Math.random() * length--);
    t = copy[length];
    copy[length] = copy[i];
    copy[i] = t;
  }
  return copy;
}
