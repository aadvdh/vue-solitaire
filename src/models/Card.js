class Card {
  constructor(suit, name, value, id) {
    this.suit = suit;
    this.value = value;
    this.name = name;
    this.id = id;
    this.isDown = true;
  }
  flipCard() {
    this.isDown = !this.isDown;
  }
  isDown() {
    return this.isDown;
  }
}

export default Card;
