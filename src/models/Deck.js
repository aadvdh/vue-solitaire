import { SUITS, VALUES } from "../utils/constants";
import Card from "./Card";
import { shuffle } from "../utils/helpers";
class Deck {
  constructor() {
    this.deck = [];
    SUITS.forEach((suit, i) => {
      for (let [key, value] of Object.entries(VALUES)) {
        let card = new Card(suit, key, value, (i + 1) * value);
        this.deck.push(card);
      }
    });
  }

  deal() {
    return this.deck.pop();
  }
  shuffle() {
    this.deck = shuffle(this.deck);
    return this;
  }
}

export default Deck;
