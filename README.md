# vue-patience

Patience game ook wel beter bekend als Solitaire gemaakt met Vue.  
Er is gebruik gemaakt van de native html drag and drop api. Hier zitten echter wel een aantal nadelen aan.  
Touch support is niet aanwezig en implementeren van multi drag is erg lastig.

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
